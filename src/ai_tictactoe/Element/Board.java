/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai_tictactoe.Element;

import ai_tictactoe.Static.Constant;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author bashimaislam
 */
public class Board {

    public int[][] block;
    public int huresticValue;
    public ArrayList<Board> neighbour = new ArrayList<>();

    public Board() {
        block = new int[Constant.BOARDSIZE][Constant.BOARDSIZE];
        for (int row = 0; row < Constant.BOARDSIZE; row++) {
            for (int column = 0; column < Constant.BOARDSIZE; column++) {
                block[row][column] = Constant.EMPTY;
            }
        }
    }

    public int whoWin() {
        //row
        for (int i = 0; i < Constant.BOARDSIZE; i++) {
            if ((block[i][0] == (block[i][1])) && (block[i][0] == block[i][2])
                    && !(block[i][0] == Constant.EMPTY)) {
                return block[i][0];
            }
        }
        // column
        for (int j = 0; j < Constant.BOARDSIZE; j++) {
            if ((block[0][j] == block[1][j]) && (block[2][j] == block[0][j])
                    && !(block[0][j] == Constant.EMPTY)) {
                return block[0][j];
            }
        }
        // left diagonal
        if ((block[0][0] == block[1][1]) && (block[1][1] == block[2][2])
                && !(block[1][1] == Constant.EMPTY)) {
            return block[1][1];
        } else if ((block[0][2] == block[1][1]) && (block[1][1] == block[2][0])
                && !(block[1][1] == Constant.EMPTY)) {
            return block[1][1];
        }
        return Constant.EMPTY;
    }

    public boolean isWin() {
        //row
        for (int i = 0; i < Constant.BOARDSIZE; i++) {
            if ((block[i][0] == (block[i][1])) && (block[i][0] == block[i][2])
                    && !(block[i][0] == Constant.EMPTY)) {
                return true;
            }
        }
        // column
        for (int j = 0; j < Constant.BOARDSIZE; j++) {
            if ((block[0][j] == block[1][j]) && (block[2][j] == block[0][j])
                    && !(block[0][j] == Constant.EMPTY)) {
                return true;
            }
        }
        // left diagonal
        if ((block[0][0] == block[1][1]) && (block[1][1] == block[2][2])
                && !(block[1][1] == Constant.EMPTY)) {
            return true;
        } else if ((block[0][2] == block[1][1]) && (block[1][1] == block[2][0])
                && !(block[1][1] == Constant.EMPTY)) {
            return true;
        }
        return false;
    }

    public boolean isFull() {
        for (int row = 0; row < Constant.BOARDSIZE; row++) {
            for (int column = 0; column < Constant.BOARDSIZE; column++) {
                if (block[row][column] == Constant.EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isEmpty() {
        for (int row = 0; row < Constant.BOARDSIZE; row++) {
            for (int column = 0; column < Constant.BOARDSIZE; column++) {
                if (!(block[row][column] == Constant.EMPTY)) {
                    return false;
                }
            }
        }
        return true;
    }

    public ArrayList<Board> findNeighbours(int player) {
        ArrayList<Board> neighbours = new ArrayList<>();
        for (int row = 0; row < Constant.BOARDSIZE; row++) {
            for (int column = 0; column < Constant.BOARDSIZE; column++) {
                if (this.block[row][column] == 0) {

                    int[][] Block = new int[Constant.BOARDSIZE][Constant.BOARDSIZE];
                    for (int i = 0; i < Constant.BOARDSIZE; i++) {
                        Block[i] = Arrays.copyOf(block[i], Constant.BOARDSIZE);
                    }

                    Block[row][column] = player;
                    Board tempBoard = new Board();
                    tempBoard.block = Block;
                    neighbours.add(tempBoard);
                }
            }
        }
        return neighbours;

    }

    public Board listMax(ArrayList<Board> boardArray) {
        Board max = boardArray.get(0);
        int size = boardArray.size();
        for (int i = 0; i < size; i++) {
            if (max.huresticValue < boardArray.get(i).huresticValue) {
                max = boardArray.get(i);
            }
        }
        return max;
    }
}
