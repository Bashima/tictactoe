/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai_tictactoe.ai;

import ai_tictactoe.Element.Board;
import ai_tictactoe.Static.Constant;
import ai_tictactoe.Static.Variable;
import static java.lang.System.exit;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author bashimaislam
 */
public class AlphaBeta {
    
    public AlphaBeta() {
    }
    
    public int calculateHurestic(Board board) {
        int value = board.whoWin();
        if (value == Constant.COMPUTER) {
            return 1;
        }
        if (value == Constant.HUMAN) {
            return -1;
        }
        return 0;
    }
    
    public void playerHuman(Board board) {
        int humanInputRow = Variable.rowS;
        int humanInputColumn = Variable.columnS;
        board.block[humanInputRow][humanInputColumn] = Constant.HUMAN;
        Variable.game.outputBoard(board);
        if (board.isWin()) {
            JOptionPane.showMessageDialog(null, "You Win");
            exit(0);
        } else if (board.isFull()) {
            JOptionPane.showMessageDialog(null, "This is a Draw");
            exit(0);
        } else {
            playerComputer(board);
        }
        
    }
    
    public void playerComputer(Board board) {
        Board maxBoard = new Board();
        maxBoard.block = board.block;
        maximumFunction(maxBoard, Integer.MIN_VALUE, Integer.MAX_VALUE);
        Board tempBoard = board.listMax(maxBoard.neighbour);
        
        board.block = tempBoard.block;
        Variable.game.outputBoard(board);
        if (board.isWin()) {
            JOptionPane.showMessageDialog(null, "You Lose");
            exit(0);
        } else if (board.isFull()) {
            JOptionPane.showMessageDialog(null, "This is a Draw");
            exit(0);
        }

    }

    public int maximumFunction(Board board, int alpha, int beta) {
        if (board.isWin()) {
            return calculateHurestic(board);
        } else if (board.isFull()) {
            return Constant.EMPTY;
        } else {
            int max = Constant.MINUSINFINITY;
            ArrayList<Board> neighbours = board.findNeighbours(Constant.COMPUTER);
            int temp;
            for (Board neighbour : neighbours) {
                temp = minimumFunction(neighbour, alpha, beta);
                temp = Math.max(max, temp);
                if (temp != max) {
                    max = Math.max(max,temp);
                }
                neighbour.huresticValue = temp;
                board.neighbour.add(neighbour);
                if (max >= beta) {
                    return max;
                }
                alpha = Math.max(max, alpha);
            }
            return max;
        }
    }
    
    public int minimumFunction(Board board, int alpha, int beta) {
        if (board.isWin()) {
            return calculateHurestic(board);
        } else if (board.isFull()) {
            return 0;
        } else {
            int min;
            min = Constant.PLUSINFINITY;
            ArrayList<Board> neighbours = board.findNeighbours(Constant.HUMAN);
            int temp;
            for (Board neighbour : neighbours) {
                temp = maximumFunction(neighbour, alpha, beta);
                if (temp != min) {
                    min = Math.min(min,temp);
                }
                if (min <= alpha) {
                    return min;
                }
                beta = Math.min(min, beta);
            }
            return min;
        }
    }
}
