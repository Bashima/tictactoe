/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai_tictactoe.Static;

/**
 *
 * @author bashimaislam
 */
public class Constant {

    public static final int BOARDSIZE = 3;
    public static final String CROSS = "X";
    public static final String CIRCLE = "O";
    public static final int EMPTY = 0;
    public static final int NONE = 0;
    public static final int COMPUTER = 1;
    public static final int HUMAN = 2;
    public static final int PLUSINFINITY = 1000000000;
    public static final int MINUSINFINITY = -1000000000;
}
